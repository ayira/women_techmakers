from django.apps import AppConfig


class EventsConfig(AppConfig):
    name = "women_techmakers.backend.events"
    verbose_name = 'events'
