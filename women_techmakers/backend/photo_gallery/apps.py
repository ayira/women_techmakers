from django.apps import AppConfig


class PhotoGalleryConfig(AppConfig):
    name = "women_techmakers.backend.photo_gallery"
    verbose_name = 'photo gallery'
