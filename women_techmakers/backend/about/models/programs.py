from django.db import models
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from modelcluster.models import ParentalKey
from women_techmakers.backend.utils.models import CardBlurb


class ProgramTile(CardBlurb, Orderable):
    page = ParentalKey('about.Program', related_name='program_tiles')


class Program(Page):
    intro = RichTextField(blank=True)
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    content_panels = Page.content_panels + [
        FieldPanel('intro'),
        MultiFieldPanel(
            [InlinePanel('program_tiles')],
            heading="Program Tiles", classname="collapsible collapsed"
        )
    ]

    promote_panels = Page.promote_panels + [
        MultiFieldPanel(
            [ImageChooserPanel('feed_image')],
            heading="Media Feed"
        )
    ]
