from django.db import models
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from modelcluster.models import ParentalKey
from women_techmakers.backend.utils.models import CardBlurb, RelatedLink


class CommunityCard(Orderable, CardBlurb):
    page = ParentalKey('about.Community', related_name='community_cards')


class MoreButton(RelatedLink):
    page = ParentalKey('about.Community', related_name='more_button')


class Community(Page):
    intro = RichTextField(blank=True)

    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel('intro'),
                InlinePanel('more_button')
            ],
            heading='hero section', classname="collapsible collapsed"
        ),
        MultiFieldPanel(
            [InlinePanel('community_cards')],
            heading="Communities Section", classname="collapsible collapsed"
        )
    ]

    promote_panels = Page.promote_panels + [
        MultiFieldPanel(
            [ImageChooserPanel('feed_image')],
            heading="Media Feeds"
        )
    ]
